# Getting Started with Create React App
### `npm start`
### `npm run build`

# Task 2

## Component Tree Structure:

- App
  - Router
    - Error404
    - PublicLayout
        - Form1
            - Form
                - Field
                - Field
    - PublicLayout
        - Form2
            - Form
                - Field
                - Field
                - Field

## Reusable components:
- Form: component to render form. This component receive handler of submitting, can check input data and send request to backend.
- Field: component to render field of form. This component receive child component and render one. Also this component can render message of error or helper text.

## List of components with description
- App: Main application component responsible for rendering the entire application layout.
- Router: Component responsible for routing in our app.
- Error404: Error page.
- PublicLayout: Layout for page that accessible everyone (there is also private layout in real application).
- Form1: Component for subtask1.
- Form1: Component for subtask2.
- Form: Component responsible for rendering form.
- Field: Component responsible for rendering field of form.
