import * as yup from "yup";

export const formSchema = yup.object().shape({
    name: yup.string().typeError("Required.").required("Required."),
    password: yup.string().typeError("Required.").required("Required."),
    gender: yup.number().nullable(),
});
