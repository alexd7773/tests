/**
 * external libs
 */
import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
/**
 * components
*/
import Field from "./../../common-components/field/Field";
/**
 * utils
 */
import { formSchema } from "./yup.form1";
/**
 * types
*/
import { FormType, FormProps } from "../../types";

const genderOptions = [
    {
        id: 1,
        label: 'M'
    },
    {
        id: 2,
        label: 'W'
    }
];

const Form: React.FC<FormProps> = ({ submitHandler, withGender = false }) => {
    const {
        handleSubmit,
        control,
        formState: { errors },
    } = useForm<FormType>({
        mode: "onBlur",
        resolver: yupResolver<FormType>(formSchema),
        defaultValues: {
            name: "",
            password: "",
            ...(withGender ? {gender: 1} : {}),
        },
    });

    return (
        <form
            noValidate
            autoComplete="off"
            onSubmit={handleSubmit(submitHandler)}
            className="flex"
        >
            <Controller
                name="name"
                control={control}
                render={({ field }) => (
                    <Field
                        label="Name"
                        error={errors[field.name]?.message}
                    >
                        <input
                            placeholder="Name"
                            value={field.value}
                            type="text"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.onChange((e.target.value || "").trim())}
                        />
                    </Field>
                )}
            />

            <Controller
                name="password"
                control={control}
                render={({ field }) => (
                    <Field
                        label="Password"
                        error={errors[field.name]?.message}
                    >
                        <input
                            placeholder="Password"
                            value={field.value}
                            type="password"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => field.onChange((e.target.value || "").trim())}
                        />
                    </Field>
                )}
            />

            {
                withGender &&
                <Controller
                    name="gender"
                    control={control}
                    render={({ field }) => (
                        <Field
                            label="Gender"
                            error={errors[field.name]?.message}
                        >
                            <select
                                value={field.value as number}
                                onChange={(e: React.ChangeEvent<HTMLSelectElement>) => field.onChange(e.target.value)}
                            >
                                {
                                    genderOptions.map((genderOption =>
                                        <option key={genderOption.id} value={genderOption.id}>
                                            {genderOption.label}
                                        </option>
                                    ))
                                }
                            </select>
                        </Field>
                    )}
                />
            }

            <button type="submit">Submit</button>
        </form>
    );
};

export default Form;