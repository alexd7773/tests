/**
 * external libs
*/
import React, { PropsWithChildren } from "react";
/**
 * types
*/
import { FieldProps } from './../../types';

const Field: React.FC<PropsWithChildren & FieldProps> = ({ children, label, error, helper }) => {
    return (
        <div className="flex">
            {
                !!label &&
                <p>{label}</p>
            }

            {children}

            {
                !!error &&
                <p style={{ color: "red" }}>{error}</p>
            }

            {
                !error && !!helper &&
                <p>{helper}</p>
            }
        </div>
    );
};

export default Field; 