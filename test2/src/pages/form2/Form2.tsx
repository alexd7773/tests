/**
 * external libs
 */
import React from "react";
import { SubmitHandler } from "react-hook-form";
/**
 * components
*/
import Form from "./../../common-components/form/Form";
/**
 * types
*/
import { FormType } from "./../../types";


const Form2: React.FC = () => {
    const submitHandler: SubmitHandler<FormType> = async (formdata) => {
        console.log("formdata of Form2", formdata);
    }

    return (
        <Form submitHandler={submitHandler} withGender={true} />
    );
};

export default Form2;