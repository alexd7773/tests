/**
 * external libs
*/
import React, { lazy, Suspense } from 'react';
/**
 * components
*/
const Form2 = lazy(() => import("./Form2"));

const Form2Component = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode }) => {
    return (
        <Suspense fallback={null}>
            <Form2 {...props} />
        </Suspense>
    );
};

export default Form2Component;