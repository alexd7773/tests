/**
 * external libs
*/
import React, { lazy, Suspense } from 'react';
/**
 * components
*/
const Form1 = lazy(() => import("./Form1"));

const Form1Component = (props: JSX.IntrinsicAttributes & { children?: React.ReactNode }) => {
    return (
        <Suspense fallback={null}>
            <Form1 {...props} />
        </Suspense>
    );
};

export default Form1Component;