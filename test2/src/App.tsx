/**
 * external libs
*/
import { HelmetProvider } from 'react-helmet-async';
/**
 * components
*/
import Router from './routers/Router';

function App() {
    return (
        <HelmetProvider>
            <Router />
        </HelmetProvider>
    );
}

export default App;
