import { SubmitHandler } from "react-hook-form";

export type RouteType = {
    path: string;
    exact: boolean;
    component: React.FC<any> & { preload?: () => void };
    title?: string;
    description?: string;
    seoKeys?: string[];
};

export type FormType = {
    name: string;
    password: string;
    gender?: number | null;
};

export type FormProps = {
    submitHandler: SubmitHandler<FormType>;
    withGender?: boolean;
};

export type FieldProps = {
    label?: string;
    error?: string;
    helper?: string;
};
