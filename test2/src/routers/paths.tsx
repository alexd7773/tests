/**
 * components
*/
import Form1 from "./../pages/form1/Form1Lazy";
import Form2 from "./../pages/form2/Form2Lazy";
/**
 * types
*/
import { RouteType } from "../types";

export const publicRoutes: RouteType[] = [
    { path: "/", exact: true, component: Form1, title: "Form1", description: "Form1" },
    { path: "/form2", exact: true, component: Form2, title: "Form2", description: "Form2" },
];

